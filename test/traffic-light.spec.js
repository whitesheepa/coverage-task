const { getTrafficHEXColor } = require("../src/traffic-light");
const expect = require("chai").expect;

describe("Traffic lights", () => {
  it("Test traffic-light possibilities", () => {
    expect(getTrafficHEXColor("GREEN")).to.equal("#00FF00");
    expect(getTrafficHEXColor("YELLOW")).to.equal("#FFFF00");
    expect(getTrafficHEXColor("RED")).to.equal("#FF0000");
    expect(getTrafficHEXColor("non-color")).to.equal(undefined);
  });
});
