const { runTrafficLight } = require("../src/main");
const expect = require("chai").expect;

describe("Trafic lights control", () => {
  it("Test traffic lights control correctness", () => {
    expect(runTrafficLight("GO")).to.equal("#00FF00");
    expect(runTrafficLight("WAIT")).to.equal("#FFFF00");
    expect(runTrafficLight("STOP")).to.equal("#FF0000");
    expect(runTrafficLight("non-color")).to.equal(undefined);
  });
});
