const { getTrafficHEXColor } = require("./traffic-light");

const runTrafficLight = (word) => {
  if (word == "GO") {
    result = getTrafficHEXColor("GREEN");
    return result;
  } else if (word == "WAIT") {
    result = getTrafficHEXColor("YELLOW");
    return result;
  } else if (word == "STOP") {
    result = getTrafficHEXColor("RED");
    return result;
  } else {
    return undefined;
  }
};
module.exports = { runTrafficLight };
